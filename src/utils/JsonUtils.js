export function isEmptyObj(json) {
  if (typeof json === 'undefined') return true;
  if (Object.keys(json).length === 0) return true;
  else return false;
}
export function limitWords(str, limitTo) {
  if (typeof str === 'undefined' || str === null) return '';
  //exclude  start and end white-space
  let res = str.replace(/(^\s*)|(\s*$)/gi, '');
  //convert 2 or more spaces to 1
  res = res.replace(/[ ]{2,}/gi, ' ');
  // exclude newline with a start spacing
  res = res.replace(/\n /, '\n');
  const word_arr = res.split(' ');
  if (limitTo >= word_arr.length) return str;
  else {
    const new_arr = word_arr.splice(0, limitTo);
    return new_arr.join(' ') + '...';
  }
}
export function limitLength(str, countLength) {
  if (typeof str === 'undefined' || str === null) return '';
  if (countLength >= str.length) return str;
  let res = '';
  res = str.substring(0, countLength);

  for (let i = countLength; i < str.length; i++) {
    const char = str.charAt(i);

    if (char != ' ') res += char;
    else break;
  }
  return res + '...';
}
export function getRandomInt(max) {
  return Math.floor(Math.random() * Math.floor(max));
}
export function isEmptyObjByAttr(json, attr) {
  if (typeof json === 'undefined') return true;
  if (Object.keys(json).length === 0) return true;
  else {
    if (json[attr] !== undefined && json[attr] !=='') return false;
  }
  return true;
}
