import React, {useState, useEffect} from 'react';
import {Image, Platform} from 'react-native';
import {
  Container,
  Header,
  Title,
  Item,
  Input,
  Button,
  Left,
  View,
  Icon,
  Right,
  Body,
  Text,
} from 'native-base';
import {doUserLogout} from '../actions/appAction';
import {useSelector, useDispatch} from 'react-redux';

const HeaderComponent = ({navigation, searchFilm}) => {
  const [keyword, setKeyword] = useState('');
  const dispatch = useDispatch();
  function handleLogout() {
    dispatch(doUserLogout());
    navigation.navigate('Login');
  }

  const doSearchFilm = () => {
    /*     props.navigation.navigate('SearchList', {
      keyword: keyword,
    }); */
    console.debug('doSearchFilm', keyword);
    navigation.navigate({
      routeName: 'FilmSearch',
      params: {keyword: keyword},
    });
  };
  return (
    <React.Fragment>
      <Header>
        <Left>
          <Image
            source={require('./../static/logo_04A.png')}
            style={{width: 30, height: 30}}
          />
        </Left>
        <Body>
          <Title>Thymovie</Title>
        </Body>
        <Right>
          <Button iconLeft transparent onPress={handleLogout}>
            <Icon name="log-out" />
          </Button>
        </Right>
      </Header>
    </React.Fragment>
  );
};
export default HeaderComponent;
