import React from 'react';
import {
  DeckSwiper,
  Card,
  CardItem,
  Thumbnail,
  Text,
  Left,
  Body,
} from 'native-base';

import {Image} from 'react-native';
import {
  IMG_LARGE_FILM,
  IMG_LARGE_SERIE,
  IMG_SERIE,
  IMG_FILM,
  LIMIT_WORD_HOME,
  LIMIT_LENGTH_HOME,
} from './../constants/config';
import {limitLength} from './../utils/JsonUtils';
import {sliderWidth, slideHeight} from './../styles/SliderEntry.style';
const DeskSwiperHome = props => {
  const handleGoToDetail = item => {
    // console.debug(item);
    props.navigation.navigate('FilmDetail', {
      filmId: item.id,
      title: item.title,
      fromRouteName: 'FilmSwiper',
    });
  };

  return (
    <DeckSwiper
      dataSource={props.filmList}
      renderItem={item => (
        <Card style={{elevation: 3}}>
          <CardItem button={true} onPress={() => handleGoToDetail(item)}>
            <Left>
              <Thumbnail
                source={{
                  uri: `${
                    item.film_type_id === 12 ? IMG_LARGE_SERIE : IMG_LARGE_FILM
                  }${item.idcine}/${item.images_path}`,
                }}
              />
              <Body>
                <Text>{item.title}</Text>
              </Body>
            </Left>
          </CardItem>
          <CardItem cardBody  button={true} onPress={() => handleGoToDetail(item)}>
            <Image
              style={{height: slideHeight * 1.1, flex: 1}}
              source={{
                uri: `${
                  item.film_type_id === 12 ? IMG_LARGE_SERIE : IMG_LARGE_FILM
                }${item.idcine}/${item.images_path}`,
              }}
            />
          </CardItem>
          <CardItem  button={true} onPress={() => handleGoToDetail(item)}>
            <Text>{limitLength(item.synopsis, LIMIT_LENGTH_HOME)}</Text>
          </CardItem>
        </Card>
      )}
    />
  );
};
export default DeskSwiperHome;
