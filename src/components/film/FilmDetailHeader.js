import React, {useState, useEffect} from 'react';
import {
  Header,
  Left,
  Button,
  Body,
  Title,
  Right,
  Icon,
  Text,
  Form,
  Content,
  List,
  ListItem,
  Spinner,
} from 'native-base';
import {sliderWidth, slideHeight} from './../../styles/SliderEntry.style';

import {View, Modal} from 'react-native';
import {useSelector, useDispatch} from 'react-redux';
import {addWishlistLine} from './../../actions/appAction';
import {isEmptyObj} from './../../utils/JsonUtils';
const FilmDetailHeader = props => {
  const appState = useSelector(state => state.appReducer);
  const [heartOutline, setHeartOutline] = useState(false);
  const [wishlistItem, setWishlistItem] = useState({});

  const [displayModal, setDisplayModal] = useState(false);

  useEffect(() => {
    setHeartOutline(canDisplayHeartOutline);
    setWishlistItem(displayWishlistForFilm);
  }, [props.filmId, heartOutline]);
  const dispatch = useDispatch();

  const handleBackButton = () => {
    const fromRoute = props.fromRouteName;
    console.log('handleBackButton', fromRoute);
    props.navigation.navigate(fromRoute, {
      goBack: true,
    });
  };

  const canDisplayHeartOutline = () => {
    const idFilm = props.filmId;
    if (!Array.isArray(appState.userApp.wishlists)) return false;
    if (appState.userApp.wishlists.length == 0) return false;
    for (let i = 0; i < appState.userApp.wishlists.length; i++) {
      const wl = appState.userApp.wishlists[i];
      for (let j = 0; j < wl.wistlist_lines.length; j++) {
        const item = wl.wistlist_lines[j];
        if (item.id == idFilm) return false;
      }
    }
    return true;
  };

  const displayWishlistForFilm = () => {
    const idFilm = props.filmId;
    if (!Array.isArray(appState.userApp.wishlists)) return {};
    if (appState.userApp.wishlists.length == 0) return {};
    for (let i = 0; i < appState.userApp.wishlists.length; i++) {
      const wl = appState.userApp.wishlists[i];
      for (let j = 0; j < wl.wistlist_lines.length; j++) {
        const item = wl.wistlist_lines[j];
        if (item.id == idFilm) {
          console.debug('FilmDetailHeader  wl', wl.label);
          return wl;
        }
      }
    }
    return {};
  };

  const handleAddFilmToWishlist = async idWl => {
    console.log('handleAddFilmToWishlist');
    await dispatch(addWishlistLine(props.filmId, idWl, appState.tokenApp));
    setDisplayModal(false);
    setHeartOutline(false);
  };

  return (
    <React.Fragment>
      {!isEmptyObj(appState.userApp) && (
        <React.Fragment>
          <Header>
            <Left>
              <Button transparent onPress={handleBackButton}>
                <Icon name="arrow-back" />
              </Button>
            </Left>
            <Body>
              <Title>{props.title}</Title>
            </Body>
            <Right>
              {heartOutline && (
                <Button transparent onPress={() => setDisplayModal(true)}>
                  <Icon name="heart" type="SimpleLineIcons" />
                </Button>
              )}
              {!heartOutline && !isEmptyObj(wishlistItem) && (
                <Button transparent iconLeft>
                  <Icon name="heart" />
                  <Text>{wishlistItem.label}</Text>
                </Button>
              )}
            </Right>
          </Header>

          <Modal transparent={true} animationType="fade" visible={displayModal}>
            {Array.isArray(appState.userApp.wishlists) && (
              <View
                style={{
                  flex: 1,
                  backgroundColor: 'rgba(0,0,0,0.7)',
                  justifyContent: 'flex-start',
                  alignItems: 'center',
                  marginTop: 50,
                }}>
                <View
                  style={{
                    flex: 1,
                    backgroundColor: 'white',
                    width: sliderWidth * 0.8,
                  }}>
                  <List>
                    {appState.userApp.wishlists.map(item => {
                      return (
                        <ListItem selected key={item.id}>
                          <Left>
                            <Text>
                              {item.label} ({item.wistlist_lines.length})
                            </Text>
                          </Left>
                          <Right>
                            <Button
                              transparent
                              onPress={() => handleAddFilmToWishlist(item.id)}>
                              <Icon name="heart" />
                            </Button>
                          </Right>
                        </ListItem>
                      );
                    })}
                    {appState.isLoading && <Spinner />}
                  </List>
                </View>
                <Button
                  style={{marginTop: 15}}
                  rounded
                  light
                  onPress={() => setDisplayModal(false)}>
                  <Text>Fermer</Text>
                </Button>
              </View>
            )}
          </Modal>
        </React.Fragment>
      )}
    </React.Fragment>
  );
};
export default FilmDetailHeader;
