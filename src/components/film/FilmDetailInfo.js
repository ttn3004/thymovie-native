import React from 'react';
import {
  Container,
  Content,
  Text,
  Button,
  ListItem,
  Icon,
  Left,
  Body,
  Right,
  Switch,
  Badge,
  Separator,
} from 'native-base';
const FilmDetailInfo = props => {
  console.debug('FilmDetailInfo', props.filmItem.original_title);
  return (
    <Container>
      <Content>
        <Separator bordered>
          <Text>Titre original</Text>
        </Separator>
        <ListItem>
          <Text>
            {props.filmItem.original_title
              ? props.filmItem.original_title
              : props.filmItem.title}
          </Text>
        </ListItem>
        <Separator bordered>
          <Text>Réalisation</Text>
        </Separator>
        {props.filmItem.director.map(director => {
          return (
            <ListItem key={director.id}>
              <Text>{director.name}</Text>
            </ListItem>
          );
        })}
        <Separator bordered>
          <Text>Acteurs principaux</Text>
        </Separator>
        {props.filmItem.actor.map(actor => {
          return (
            <ListItem key={actor.id}>
              <Text>{actor.name}</Text>
            </ListItem>
          );
        })}
        <Separator bordered>
          <Text>Pays d'origine</Text>
        </Separator>
        {props.filmItem.country.map(country => {
          return (
            <ListItem key={country.id}>
              <Text>{country.name}</Text>
            </ListItem>
          );
        })}
        <Separator bordered>
          <Text>Pays d'origine</Text>
        </Separator>
        <ListItem>
          <Text>{props.filmItem.year_production}</Text>
        </ListItem>
        <Separator bordered>
          <Text>Durée (minutes)</Text>
        </Separator>
        <ListItem>
          <Text>{props.filmItem.duration}</Text>
        </ListItem>
      </Content>
    </Container>
  );
};
export default FilmDetailInfo;
