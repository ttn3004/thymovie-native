import React, {useState, useEffect, useRef} from 'react';
import VideoPlayer from 'react-native-video-player';
import {View} from 'react-native';
import {Button, Content, Text, Icon} from 'native-base';
import {VIDEO_URL} from './../../constants/config';
const FilmDetailVideo = props => {
  const [playPressed, setPlayPressed] = useState(false);
  const [urlVid, setUrlVid] = useState('');
  const [errorOnLoad, setErrorOnLoad] = useState(false);
  const videoEl = useRef();

  useEffect(() => {
    const res = getUrlVideo();
    console.log('FilmDetailVideo', res);
    setUrlVid(res);
  }, [props.filmItem]);

  function getUrlVideo() {
    const arrVid = props.filmItem.film_videos;
    let url = '';
    for (let i = 0; i < arrVid.length; i++) {
      const vid = arrVid[i];
      if (vid.vid_med) {
        url = `${VIDEO_URL}${props.filmItem.idcine}/medium/${vid.vid_med}`;
      }
      if (vid.vid_high) {
        url = `${VIDEO_URL}${props.filmItem.idcine}/high/${vid.vid_high}`;
      }
    }
    return url;
  }
  const handlePressPause = () => {
    videoEl.current.pause();
  };
  const handlePressResume = () => {
    videoEl.current.resume();
  };
  const handlePressStop = () => {
    setTimeout(() => videoEl.current.seek(0), 0);
    videoEl.current.pause();
  };
  return (
    <React.Fragment>
      {errorOnLoad && (
        <View
          style={{
            flex: 1,
            marginTop: 10,
            justifyContent: 'center',
            alignItems: 'center',
          }}>
          <Icon name="error" type="MaterialIcons"/>
          <Text>Error on load video url</Text>
        </View>
      )}
      {!errorOnLoad && (
        <View
          style={{
            flex: 1,
            flexDirection: 'column',
          }}>
          <VideoPlayer
            video={{
              uri: urlVid,
            }} // Can be a URL or a local file.
            ref={videoEl} // Store reference
            onStart={() => setPlayPressed(true)}
            onError={() => setErrorOnLoad(true)}
            disableFullscreen={false}
          />
          <Content>
            <View
              style={{
                flex: 1,
                marginTop: 10,
                flexDirection: 'row',
                justifyContent: 'space-around',
              }}>
              {playPressed && (
                <Button rounded bordered danger onPress={handlePressStop}>
                  <Icon name="stop" type="Foundation" />
                </Button>
              )}
              <Button rounded bordered warning onPress={handlePressPause}>
                <Icon name="pause" type="Feather" />
              </Button>

              <Button rounded bordered onPress={handlePressResume}>
                <Icon name="reload1" type="AntDesign" />
              </Button>
            </View>
          </Content>
        </View>
      )}
    </React.Fragment>
  );
};
export default FilmDetailVideo;
