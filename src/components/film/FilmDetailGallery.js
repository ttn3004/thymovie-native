import React from 'react';
import {
  Container,
  Header,
  View,
  DeckSwiper,
  Card,
  CardItem,
  Thumbnail,
  Text,
  Left,
  Body,
  Icon,
} from 'native-base';
import {
  IMG_LARGE_FILM,
  IMG_LARGE_SERIE
} from './../../constants/config';
import {Image} from 'react-native';
import {sliderWidth, slideHeight} from './../../styles/SliderEntry.style';

const FilmDetailGallery = props => {
  return (
    <Container>
      <View style={{flex: 1, marginTop: 5}}>
        <DeckSwiper
          dataSource={props.filmItem.film_images_large}
          renderItem={item => (
            <Image
              style={{height: slideHeight, flex: 1}}
              source={{
                uri: `${
                  props.filmItem.film_type_id === 12
                    ? IMG_LARGE_SERIE
                    : IMG_LARGE_FILM
                }${props.filmItem.idcine}/${item.images_path}`,
              }}
            />
          )}
        />
      </View>
    </Container>
  );
};
export default FilmDetailGallery;
