import React, {useState, useEffect} from 'react';
import {
  Container,
  Header,
  Left,
  Button,
  Body,
  Title,
  Right,
  Text,
  Icon,
  Content,
  Tab,
  Tabs,
  TabHeading,
  Spinner,
  Card,
  CardItem,
  Thumbnail,
} from 'native-base';
import FilmDetailInfo from './FilmDetailInfo';
import FilmDetailGallery from './FilmDetailGallery';
import FilmDetailReview from './FilmDetailReview';
import FilmDetailVideo from './FilmDetailVideo';
import {
  IMG_LARGE_FILM,
  IMG_LARGE_SERIE,
  IMG_SERIE,
  IMG_FILM,
  LIMIT_WORD_HOME,
  LIMIT_LENGTH_LIST,
} from '../../constants/config';
import {sliderWidth, slideHeight} from '../../styles/SliderEntry.style';

const FilmDetail = props => {
  console.debug('FilmDetail', props.filmItem.id);
  return (
    <Tabs locked={true}>
      <Tab
        heading={
          <TabHeading>
            <Text style={{textAlign: 'center'}}>Synopsis</Text>
          </TabHeading>
        }>
        <FilmDetailGallery filmItem={props.filmItem} />
        <Container>
          <Content padder>
            <Card transparent>
              <CardItem>
                <Body>
                  <Text>{props.filmItem.synopsis} </Text>
                </Body>
              </CardItem>
            </Card>
          </Content>
        </Container>
      </Tab>
      <Tab
        heading={
          <TabHeading>
            <Text style={{textAlign: 'center'}}>Trailler</Text>
          </TabHeading>
        }>
        <FilmDetailVideo filmItem={props.filmItem} />
      </Tab>
      <Tab
        heading={
          <TabHeading>
            <Text style={{textAlign: 'center'}}>Info</Text>
          </TabHeading>
        }>
        <FilmDetailInfo filmItem={props.filmItem} />
      </Tab>
      <Tab
        heading={
          <TabHeading>
            <Text style={{textAlign: 'center'}}>Avis</Text>
          </TabHeading>
        }>
        <FilmDetailReview filmItem={props.filmItem} />
      </Tab>
    </Tabs>
  );
};
export default FilmDetail;
