import React, {useState, useEffect} from 'react';
import {useSelector, useDispatch} from 'react-redux';

import {
  Container,
  Header,
  Content,
  List,
  ListItem,
  Left,
  Body,
  Right,
  Thumbnail,
  Text,
  Button,
  Icon,
  Form,
  Item,
  Label,
  Textarea,
  Spinner,
} from 'native-base';
import moment from 'moment';
import {getRandomInt} from './../../utils/JsonUtils';
import {View, Image, Modal, TouchableHighlight} from 'react-native';
import {sliderWidth} from './../../styles/SliderEntry.style';
import {addCommentsForFilmApi} from './../../api/userApi';
const FilmDetailReview = props => {
  const avatars = [
    'https://img.icons8.com/office/40/000000/avatar.png',
    'https://img.icons8.com/plasticine/100/000000/person-male.png',
    'https://img.icons8.com/nolan/64/person-male.png',
    'https://img.icons8.com/dusk/64/000000/person-male.png',
    'https://img.icons8.com/dusk/64/000000/small-smile.png',
  ];

  function getRandomAvatar() {
    const idxRd = getRandomInt(avatars.length);
    return avatars[idxRd];
  }

  const [modeAddRev, setModeAddRev] = useState(false);
  const [isAdding, setIsAdding] = useState(false);
  const [commentList, setCommentList] = useState(props.filmItem.film_reviews);
  const [comments, setComments] = useState('');
  const appState = useSelector(state => state.appReducer);

  const handleCloseModal = () => {
    setModeAddRev(false);
    setComments('');
  };

  const handleAddComments = async () => {
    setIsAdding(true);
    try {
      const com = await addCommentsForFilmApi(
        props.filmItem.id,
        comments,
        appState.tokenApp,
      );
      let temp = commentList;
      temp.push(com);
      setCommentList(temp);
      setModeAddRev(false);
      setComments('');
      setIsAdding(false);
    } catch (error) {
      setIsAdding(false);
    } finally {
      setIsAdding(false);
    }
  };
  return (
    <Container>
      <Content>
        {Array.isArray(commentList) && (
          <List>
            {commentList.map(item => {
              const imgUrl = getRandomAvatar();
              return (
                <ListItem avatar key={item.id}>
                  <Left>
                    <Thumbnail
                      source={{
                        uri: imgUrl,
                      }}
                    />
                  </Left>
                  <Body>
                    <Text>{item.name}</Text>
                    <Text note>{item.comments}</Text>
                  </Body>
                  <Right>
                    <Text note>
                      {moment(item.updated_at, 'YYYY-MM-DD HH:mm').format(
                        'DD/MM/YY HH:mm',
                      )}
                    </Text>
                  </Right>
                </ListItem>
              );
            })}
          </List>
        )}
      </Content>
      <Content
        style={{
          position: 'absolute',
          right: 10,
          bottom: 20,
          display: 'flex',
        }}>
        <Button
          onPress={() => {
            setModeAddRev(true);
          }}
          warning
          rounded
          style={{width: 60, justifyContent: 'center'}}>
          <Icon name="comment-o" type="FontAwesome" />
        </Button>
      </Content>
      <Modal transparent={true} animationType="fade" visible={modeAddRev}>
        <View
          style={{
            flex: 1,
            backgroundColor: 'rgba(0,0,0,0.7)',
            justifyContent: 'flex-start',
            alignItems: 'center',
            flexDirection: 'column',
            marginTop: 55,
          }}>
          <Form
            style={{
              alignItems: 'center',
              backgroundColor: 'white',
              padding: 10,
            }}>
            <Textarea
              rowSpan={4}
              bordered
              placeholder="Votre commentaires"
              style={{width: sliderWidth * 0.9}}
              value={comments}
              onChangeText={label => setComments(label)}
            />

            <Button onPress={handleAddComments} style={{marginTop: 15}}>
              <Icon name="checkmark" />
              <Text>Valider</Text>
              {isAdding && <Spinner />}
            </Button>
          </Form>
          <Button
            style={{marginTop: 15}}
            rounded
            light
            onPress={handleCloseModal}>
            <Text>Fermer</Text>
          </Button>
        </View>
      </Modal>
    </Container>
  );
};
export default FilmDetailReview;
