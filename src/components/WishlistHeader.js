import React, {useState, useEffect} from 'react';
import {useSelector, useDispatch} from 'react-redux';
import {
  Container,
  Header,
  Title,
  Item,
  Input,
  Left,
  Icon,
  Right,
  Body,
  Text,
  Spinner,
  Form,
  Label,
  Button,
  Content,
} from 'native-base';
import {View, Image, Modal} from 'react-native';
import {
  doAddNewWishlist,
} from './../actions/appAction';
import {sliderWidth} from './../styles/SliderEntry.style';
export const WishlistHeader = () => {
  const [isAddMode, setAddMode] = useState(false);
  const [isAdding, setAdding] = useState(false);

  const [labelWishlist, setLabelWishlist] = useState('');
  const appState = useSelector(state => state.appReducer);

  const dispatch = useDispatch();

  const handleAddWishlist = async () => {
    console.log('handleAddWishlist');
    await dispatch(doAddNewWishlist(labelWishlist, appState.tokenApp));
    setAddMode(false);
    setAdding(false)
  };

  const handleCloseModal = () => {
    console.log('fermer modal');
    setAddMode(false);
    setAdding(false)
    setLabelWishlist('');
  };
  return (
    <React.Fragment>
      <Header>
        <Left>
          <Image
            source={require('./../static/logo_04A.png')}
            style={{width: 30, height: 30}}
          />
        </Left>
        <Body>
          <Title>Thymovie</Title>
        </Body>
        <Right>
          <Button iconRight transparent onPress={() => setAddMode(true)}>
            <Icon name="add" />
          </Button>
        </Right>
      </Header>

      <Modal transparent={true} animationType="fade" visible={isAddMode}>
        <View
          style={{
            flex: 1,
            backgroundColor: 'rgba(0,0,0,0.7)',
            justifyContent: 'flex-start',
            alignItems: 'center',
            flexDirection: 'column',
            marginTop: 55,
          }}>
          <Form
            style={{
              alignItems: 'center',
              backgroundColor: 'white',
              padding: 10,
            }}>
            <Item floatingLabel  style={{width: sliderWidth * 0.8}}>
              <Label>Nom de la liste</Label>
              <Input
               
                value={labelWishlist}
                onChangeText={label => setLabelWishlist(label)}
              />
            </Item>
            <Button onPress={() => handleAddWishlist} style={{marginTop: 15}}>
              <Icon name="checkmark" />
              <Text>Valider</Text>
              {isAdding && <Spinner />}
            </Button>
          </Form>
          <Button
            style={{marginTop: 15}}
            rounded
            light
            onPress={handleCloseModal}>
            <Text>Fermer</Text>
          </Button>
        </View>
      </Modal>
    </React.Fragment>
  );
};
export default WishlistHeader;
