import React, {
  useState,
  useEffect,
  forwardRef,
  useImperativeHandle,
} from 'react';
import {useSelector, useDispatch} from 'react-redux';
import {
  Container,
  Header,
  Content,
  List,
  ListItem,
  Thumbnail,
  Text,
  Left,
  Body,
  Right,
  Button,
  Icon,
  Spinner,
} from 'native-base';
import {View, Image, Modal} from 'react-native';

import {
  IMG_LARGE_FILM,
  IMG_LARGE_SERIE,
  IMG_SERIE,
  IMG_FILM,
  LIMIT_WORD_HOME,
  LIMIT_LENGTH_LIST,
} from './../constants/config';
import {isEmptyObj, limitLength} from './../utils/JsonUtils';
const DataPaging = ({
  fromRouteName,
  handlerNextData,
  filmArray,
  navigation,
  disableButtonNext,
}) => {
  const handleGoToDetail = item => {
    // console.debug(item);
    navigation.navigate('FilmDetail', {
      filmId: item.id,
      title: item.title,
      fromRouteName: fromRouteName,
    });
  };
  return (
    <Content>
      {filmArray.length > 0 && (
        <List>
          {filmArray.map((item, idx) => {
            return (
              <ListItem thumbnail key={item.id}>
                <Left>
                  <Thumbnail
                    square
                    source={{
                      uri: `${
                        item.film_type_id === 12
                          ? IMG_LARGE_SERIE
                          : IMG_LARGE_FILM
                      }${item.idcine}/${item.images_path}`,
                    }}
                  />
                </Left>
                <Body>
                  <Text>
                    {item.title}
                  </Text>
                  <Text note numberOfLines={1}>
                    {limitLength(item.synopsis, LIMIT_LENGTH_LIST)}
                  </Text>
                </Body>
                <Right>
                  <Button
                    iconRight
                    transparent
                    onPress={() => handleGoToDetail(item)}>
                    {/*   onPress={handleGoToDetail(item)} */}
                    <Icon name="arrow-round-forward" />
                  </Button>
                </Right>
              </ListItem>
            );
          })}
          {!disableButtonNext && (
            <Button
              full
              info
              style={{textAlign: 'center'}}
              onPress={handlerNextData}>
              <Icon name="skip-forward" />
            </Button>
          )}
        </List>
      )}
    </Content>
  );
};
export default DataPaging;
