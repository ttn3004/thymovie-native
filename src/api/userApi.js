import {TOKEN} from '../constants/config';
import axios from 'axios';
import {API_HOST} from '../constants/config';

const USER_LOGIN = 'login.php';
const USER_INFO = 'login.php?jwt=';
const USER_SAVE_WISHLIST = 'wishlist.php';
const USER_WISHLIST = 'wishlist_line.php';
const FILM_ITEM_REV = 'review.php';
const USER_REGISTER = 'spectator.php';
const myheaders = {
  'content-type': 'application/x-www-form-urlencoded;charset=utf-8',
};
//For post request
export const axiosForPost = axios.create({
  baseURL: API_HOST,
  timeout: 15000,
  headers: myheaders,
});

//login
const axiosLogin = axios.create({
  baseURL: API_HOST,
  headers: myheaders,
});
//auth
const axiosAuth = axios.create({
  baseURL: API_HOST,
});

export const loginUserApi = (email, pass) => {
  const param = {
    email: email,
    password: pass,
  };
  try {
    return axiosLogin.post(USER_LOGIN, param).then(res => {
      const token = res.data.jwt;
      if (typeof token === 'undefined') {
        throw new Error(res.data.message);
      } else {
        return token;
      }
    });
  } catch (error) {
    return error;
  }
};
export const registerUserApi = (email, pass) => {
  const param = {
    email: email,
    password: pass
  };
  try {
    return axiosLogin.post(USER_REGISTER, param).then(res => {
      const token = res.data.jwt;
      if (typeof token === 'undefined') throw new Error(res.data.message);
      return token;
    });
  } catch (error) {
    return error;
  }
};


//logout
export const logoutUserApi = () => {};

export const userInfoApi = tk => {
  try {
    return axiosAuth.get(USER_INFO + tk).then(res => res.data.data);
  } catch (error) {
    return error;
  }
};

export const addNewWishlistApi = (labelWishlist, token) => {
  try {
    const data = {
      jwt: token,
      newWl_label: labelWishlist,
    };
    return axiosForPost
      .post(USER_SAVE_WISHLIST, JSON.stringify(data))
      .then(res => res.data.data);
  } catch (error) {
    return error;
  }
};
//delete wishlist
export const deleteWishlistApi = (idWl, token) => {
  try {
    const data = {
      jwt: token,
      idWishlist: idWl,
    };
    return axiosForPost
      .delete(USER_SAVE_WISHLIST, {data: data})
      .then(res => res.data.data);
  } catch (error) {
    return error;
  }
};

//add to my list
export const addToMyListApi = (idFilm, idWl, token) => {
  try {
    const data = {
      jwt: token,
      idFilm: idFilm,
      idWistlist: idWl,
    };
    return axiosForPost
      .post(USER_WISHLIST, JSON.stringify(data))
      .then(res => res.data.data);
  } catch (error) {
    return error;
  }
};
export const deleteWishlisLinetApi = (idWl_line, token) => {
  try {
    const data = {
      jwt: token,
      idWistlist_Line: idWl_line,
    };
    return axiosForPost
      .delete(USER_WISHLIST, {data: data})
      .then(res => res.data.data);
  } catch (error) {
    return error;
  }
};
export const addCommentsForFilmApi = (filmId, comments, token) => {
  try {
    const data = {
      jwt: token,
      idFilm: filmId,
      comment: comments,
    };
    return axiosForPost
      .post(FILM_ITEM_REV, JSON.stringify(data))
      .then(res => res.data.data);
  } catch (error) {
    return error;
  }
};
