import {API_HOST} from './../constants/config';
import axios from 'axios';
import {sleep} from './../utils/TimeUtils';

const myheaders = {
  'content-type': 'application/x-www-form-urlencoded;charset=utf-8',
};

export const axiosInstance = axios.create({
  baseURL: API_HOST,
  timeout: 15000,
});
export const axiosForPost = axios.create({
  baseURL: API_HOST,
  timeout: 15000,
  headers: myheaders,
});
const FILM_MENUHOME = 'film.php/menuHome';

export const filmListMenuHomeApi = () => {
  try {
    return axiosInstance.get(FILM_MENUHOME).then(res => res.data);
  } catch (error) {
    return error;
  }
};

const FILM_PAGING = 'film_datatable.php';
export const filmsPagingApi = postObject => {
  try {
    return axiosForPost.post(FILM_PAGING, postObject).then(res => res.data);
  } catch (error) {
    return error;
  }
};
const FILM_ITEM = 'film.php/';
export const filmItemApi = idItem => {
  try {
    if (typeof idItem === 'undefined') throw new Error('idFilm is empty');
    return axiosInstance.get(FILM_ITEM + idItem).then(res => res.data);
  } catch (error) {
    throw new Error(error);
  }
};
