export const API_HOST = 'http://rest.thymovie.com/rest/';

export const TOKEN = 'token';
export const IMG_LARGE_FILM = 'http://thymovie.com/image/large/thymovies-img/';
export const IMG_LARGE_SERIE =
  'http://thymovie.com/image/large/thymovies-serie-img/';
export const VIDEO_URL='http://thymovie.com/videos/thymovies-vid/';
export const IMG_SERIE = '';
export const IMG_FILM = '';
export const LIMIT_WORD_HOME = 20;
export const LIMIT_LENGTH_HOME = 200;
export const LIMIT_LENGTH_LIST = 100;
export const LIMIT_LENGTH_WISHLIST = 100;
export const MIN_KEYWORD_SEARCH = 2;
