import {
  AUTH_START,
  AUTH_SUCCESS,
  AUTH_LOGIN_SUCCESS,
  AUTH_LOGOUT_SUCCESS,
  AUTH_FAILURE,
  USER_ACTION_START,
  USER_ACTION_FAILURE,
} from '../constants/actionTypes';
import {
  userInfoApi,
  loginUserApi,
  addToMyListApi,
  addNewWishlistApi,
  deleteWishlistApi,
  deleteWishlisLinetApi,
  addCommentsForFilmApi,
  registerUserApi,
} from '../api/userApi';
import {sleep} from './../utils/TimeUtils';
export const doAuthStart = () => {
  return {
    type: AUTH_START,
  };
};
export const doAuhFailure = error => {
  return {
    type: AUTH_FAILURE,
    error,
  };
};
export const doAuthLogoutSuccess = () => {
  return {
    type: AUTH_LOGOUT_SUCCESS,
  };
};
export const doAuthLoginSuccess = data => {
  return {
    type: AUTH_LOGIN_SUCCESS,
    error: '',
    data,
  };
};
export const doAuthSuccess = data => {
  return {
    type: AUTH_SUCCESS,
    error: '',
    data,
  };
};
const doUserRegisterFailure = error => {
  return {
    type: AUTH_FAILURE,
    error,
  };
};
export const doUserLogout = () => async dispatch => {
  dispatch(doAuthStart());
  try {
    await dispatch(doAuthLogoutSuccess());
  } catch (error) {
    dispatch(doAuhFailure(error.message));
  }
};
export const doUserLogin = (email, pass) => async dispatch => {
  dispatch(doAuthStart());
  try {
    const token = await loginUserApi(email, pass);
    const dataUser = await userInfoApi(token);
    if (dataUser.email === '') {
      dispatch(doAuhFailure('User not found'));
    } else {
      dispatch(doAuthLoginSuccess(token));
      dispatch(doAuthSuccess(dataUser));
    }
  } catch (error) {
    console.log('catch',error)
    dispatch(doAuhFailure(error.message));
  }
};

export const doUserRegister = (email, pass) => async dispatch => {
  dispatch(doAuthStart());
  try {
    if (email.length <= 3 || pass.length < 3) {
      dispatch(doUserRegisterFailure('Formulaire non valide'));
    } else {
      const token = await registerUserApi(email, pass);
      const dataUser = await userInfoApi(token);
      if (dataUser.email === '') {
        dispatch(doAuhFailure('User not found'));
      } else {
        dispatch(doAuthLoginSuccess(token));
        dispatch(doAuthSuccess(dataUser));
      }
    }
  } catch (error) {
    dispatch(doUserRegisterFailure(error.message));
  }
};

export const doUserActionStart = () => {
  return {
    type: USER_ACTION_START,
  };
};
export const doUserActionFailure = error => {
  return {
    type: USER_ACTION_FAILURE,
    error,
  };
};
export const doAddNewWishlist = (labelWishlist, token) => async dispatch => {
  dispatch(doUserActionStart());
  try {
    await addNewWishlistApi(labelWishlist, token);
    const dataUser = await userInfoApi(token);
    dispatch(doAuthSuccess(dataUser));
  } catch (error) {
    dispatch(doUserActionFailure(error.message));
  }
};

export const deleteWishlist = (idWl, token) => async dispatch => {
  dispatch(doUserActionStart());
  try {
    await deleteWishlistApi(idWl, token);
    const dataUser = await userInfoApi(token);
    dispatch(doAuthSuccess(dataUser));
    console.log(dataUser);
  } catch (error) {
    dispatch(doUserActionFailure(error.message));
  }
};
export const addWishlistLine = (idFilm, idWl, token) => async dispatch => {
  dispatch(doUserActionStart());
  try {
    await addToMyListApi(idFilm, idWl, token);
    const dataUser = await userInfoApi(token);
    dispatch(doAuthSuccess(dataUser));
  } catch (error) {
    dispatch(doUserActionFailure(error.message));
  }
};
export const deleteWishlistLine = (idWlLine, token) => async dispatch => {
  dispatch(doUserActionStart());
  try {
    await deleteWishlisLinetApi(idWlLine, token);
    const dataUser = await userInfoApi(token);
    dispatch(doAuthSuccess(dataUser));
  } catch (error) {
    dispatch(doUserActionFailure(error.message));
  }
};
