import {
  FILM_HOME_START,
  FILM_HOME_SUCCESS,
  FILM_HOME_FAILURE,
  FILM_LIST_START,
  FILM_INIT_START,
  FILM_LIST_SUCCESS,
  FILM_LIST_FAILURE,
} from '../constants/actionTypes';
import {filmListMenuHomeApi, filmsPagingApi} from './../api/filmApi';

/** HOME MENU */
const doFilmHomeStart = () => {
  return {
    type: FILM_HOME_START,
  };
};
const doFilmHomeSuccess = data => {
  return {
    type: FILM_HOME_SUCCESS,
    data,
  };
};

const doFilmHomeFailure = error => {
  return {
    type: FILM_HOME_FAILURE,
    error,
  };
};

/** FILM LIST */
const initFilmListStart =()=>{
  return {
    type: FILM_INIT_START,
  };
}
const doFilmListStart = () => {
  return {
    type: FILM_LIST_START,
  };
};
const doFilmPagingSuccess = data => {
  return {
    type: FILM_LIST_SUCCESS,
    data,
  };
};
const doFilmListFailure = error => {
  return {
    type: FILM_LIST_FAILURE,
    error,
  };
};
export const doFilmHome = () => async dispatch => {
  dispatch(doFilmHomeStart());
  try {
    const data = await filmListMenuHomeApi();
    dispatch(doFilmHomeSuccess(data));
  } catch (error) {
    dispatch(doFilmHomeFailure(error.message));
  }
};

export const doFilmList = postObject => async dispatch => {
  dispatch(doFilmListStart());
  if(postObject.page===0)
  dispatch(initFilmListStart());
  try {
    const data = await filmsPagingApi(postObject);
    //console.log(data)
    dispatch(doFilmPagingSuccess(data));
  } catch (error) {
    dispatch(doFilmListFailure(error.message));
  }
};
