import { createStore,applyMiddleware  } from 'redux';
import { persistStore } from "redux-persist";
import thunk from 'redux-thunk'
import { createLogger } from 'redux-logger';
import persistedReducer from '../reducers';
const logger = createLogger();


export const store = createStore(persistedReducer, undefined, applyMiddleware(thunk));
export const persistor = persistStore(store);
