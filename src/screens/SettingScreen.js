import React from 'react';
import {Container} from 'native-base';
import {ENTRIES1, ENTRIES2} from './../static/entries';
import styles, {sliderWidth, itemWidth} from './../styles/SliderEntry.style';
import Carousel from 'react-native-snap-carousel';
import {View, Text, Image} from 'react-native';
import HeaderComponent from './../components/HeaderComponent'
export default class SettingScreen extends React.Component {
  constructor(props) {
    super(props);
    this.state = {entries: ENTRIES1};
  }
  _renderItem({item, index}) {
    return (
      <View style={styles.slideInnerContainer}>
        <Text style={styles.title}>{item.title}</Text>
        <Image style={styles.image} source={{uri: item.illustration}} />
      </View>
    );
  }

  render() {
    return (
      <Container>
        <HeaderComponent />
        <Carousel
          ref={c => {
            this._carousel = c;
          }}
          data={this.state.entries}
          renderItem={this._renderItem}
          sliderWidth={sliderWidth}
          itemWidth={itemWidth}
        />
      </Container>
    );
  }
}
