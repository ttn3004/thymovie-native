import React from 'react';
import {
  Container,
  Header,
  View,
  DeckSwiper,
  Card,
  CardItem,
  Thumbnail,
  Text,
  Left,
  Body,
  Icon,
} from 'native-base';
import {Image} from 'react-native';
import {ENTRIES1, ENTRIES2} from './../static/entries';

const SwiperScreen = props => {
  return (
    <Container>
      <View>
        <DeckSwiper
          dataSource={ENTRIES1}
          renderItem={item => (
            <Card style={{elevation: 3}}>
              <CardItem>
                <Left>
                  <Thumbnail source={{uri: item.illustration}} />
                  <Body>
                    <Text>{item.title}</Text>
                    <Text note>{item.subtitle}</Text>
                  </Body>
                </Left>
              </CardItem>
              <CardItem cardBody>
                <Image
                  style={{height: 300, flex: 1}}
                  source={{uri: item.illustration}}
                />
              </CardItem>
              <CardItem>
                <Icon name="heart" style={{color: '#ED4A6A'}} />
                <Text>{item.title}</Text>
              </CardItem>
            </Card>
          )}
        />
      </View>
    </Container>
  );
};

export default SwiperScreen;
