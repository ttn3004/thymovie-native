import React, {useState, useEffect} from 'react';
import {useSelector, useDispatch} from 'react-redux';
import {Container, Spinner, Content, Text} from 'native-base';
import {View} from 'react-native';
import {isEmptyObj} from './../utils/JsonUtils';
const AuthLoadingScreen = props => {
  const appState = useSelector(state => state.appReducer);
  const dispatch = useDispatch();

  useEffect(() => {
    function getAuthFromStorage() {
      if (isEmptyObj(appState.userApp)) {
        props.navigation.navigate('Login');
      } else {
        props.navigation.navigate('App');
      }
    }
    getAuthFromStorage();
  });

  return (
    <Container>
      <View
        style={{
          flex: 1,
          backgroundColor: 'rgba(0,0,0,0.7)',
          justifyContent: 'center',
          alignItems: 'center',
        }}>
        <Spinner />
      </View>
    </Container>
  );
};
export default AuthLoadingScreen;
