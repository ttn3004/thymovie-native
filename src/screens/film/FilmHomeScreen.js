import React, {useState, useEffect} from 'react';
import {
  Container,
  Button,
  Text,
  Icon,
  Spinner,
  Header,
  Item,
  Input,
} from 'native-base';
import DataPaging from './../../components/DataPaging';
import HeaderComponent from './../../components/HeaderComponent';
import {doFilmList} from './../../actions/filmListAction';
import {NavigationEvents} from 'react-navigation';
import {useSelector, useDispatch} from 'react-redux';
import {View, Image, Modal} from 'react-native';
import {isEmptyObj} from './../../utils/JsonUtils';
import uuid from 'uuid';
import {MIN_KEYWORD_SEARCH} from './../../constants/config';

const FilmHomeScreen = props => {
  const [keyword, setKeyword] = useState('');
  const [typeFilm, setTypeFilm] = useState('');
  const [displaySearch, setDisplaySearch] = useState(false);
  useEffect(() => {
    console.log('useEffect', keyword);
  }, [keyword]);
  const doSearchFilm = () => {
    console.debug('doSearchFilm', keyword);
    if (keyword.length <= MIN_KEYWORD_SEARCH) return;

    const postDataTableObject = {
      ...filmList.postDataTableObject,
      listFiltered: [],
      menuFiltered: [],
      searchByKeyword: keyword,
      page: 0,
      filmType: 'all',
    };

    console.debug('doOnDidFocus', postDataTableObject);
    dispatch(doFilmList(postDataTableObject));
    setDisplaySearch(true);
  };
  const resetSearch = () => {
    console.debug('reset');
    setKeyword('');
    setDisplaySearch(false);
    /** reset value of list */
    const postDataTableObject = {
      ...filmList.postDataTableObject,
      listFiltered: [],
      menuFiltered: [],
      searchByKeyword: '',
      page: 0,
      filmType: typeFilm,
    };
    dispatch(doFilmList(postDataTableObject));
  };
  /** initial focus */
  const doOnDidFocus = payload => {
    let postDataTableObject = {};
    //console.log(payload);
    /**Case new focus */
    if (!isEmptyObj(payload.action.routeName)) {
      postDataTableObject = {
        ...filmList.postDataTableObject,
        listFiltered: [],
        menuFiltered: [],
        searchByKeyword: '',
        page: 0,
        filmType:
          payload.action.routeName === 'FilmHome' ||
          payload.action.routeName === 'FilmTable'
            ? 'film'
            : 'serie',
      };
    } else {
      /* Case go back from Detail */
      if (!isEmptyObj(payload.state.params)) {
        if (!isEmptyObj(payload.state.params.goBack)) return;
      }
    }

    console.debug('doOnDidFocus', postDataTableObject);
    try {
      setTypeFilm(postDataTableObject.filmType);
    } catch (error) {
      console.debug('not type of film');
    }
    dispatch(doFilmList(postDataTableObject));
  };
  const filmList = useSelector(state => state.filmListReducer);

  const dispatch = useDispatch();

  const getNextData = () => {
    const postDataTableObject = {
      ...filmList.postDataTableObject,
      page: filmList.postDataTableObject.page + 1,
    };
    dispatch(doFilmList(postDataTableObject));
  };
  return (
    <Container>
      <HeaderComponent navigation={props.navigation} />
      <Header searchBar rounded>
        <Item>
          <Icon name="search" />
          <Input
            placeholder="Film, série..."
            value={keyword}
            onChangeText={text => setKeyword(text)}
          />
          <Icon name="close" onPress={resetSearch} />
          <Icon name="easel" onPress={doSearchFilm} />
        </Item>
        <Button onPress={doSearchFilm}>
          <Text>Search</Text>
        </Button>
      </Header>

      <NavigationEvents onDidFocus={payload => doOnDidFocus(payload)} />
      {/* result */}
      {!filmList.isLoading && displaySearch && (
        <Text>
          Résultat(s) trouvé(s): {filmList.postDataTableObject.totalSize}
        </Text>
      )}
      {filmList.isLoading && (
        <Modal
          animationType="fade"
          transparent={true}
          visible={filmList.isLoading}>
          <View
            style={{
              flex: 1,
              backgroundColor: 'rgba(0,0,0,0.7)',
              justifyContent: 'center',
              alignItems: 'center',
            }}>
            <View>
              <Spinner />
            </View>
          </View>
        </Modal>
      )}
      {/* DataPaging  */}
      <DataPaging
        fromRouteName="FilmTable"
        handlerNextData={getNextData}
        filmArray={filmList.filmArray}
        navigation={props.navigation}
        disableButtonNext={
          filmList.postDataTableObject.page + 1 ==
          filmList.postDataTableObject.totalPage
        }
      />
    </Container>
  );
};
export default FilmHomeScreen;
