import {createStackNavigator} from 'react-navigation-stack';
import FilmHomeScreen from './FilmHomeScreen';
import FilmSwiperListScreen from './FilmSwiperListScreen';
import WishListHomeScreen from './../user/WishListHomeScreen';
import FilmDetailScreen from './../film/FilmDetailScreen';
export const FilmScreen = createStackNavigator(
  {
    FilmTable: {
      screen: FilmHomeScreen,
      navigationOptions: {
        headerShown: false,
      },
    },
    FilmDetail: {
      screen: FilmDetailScreen,
      navigationOptions: {
        headerShown: false,
      },
    },
  },
  {
    initialRouteName: 'FilmTable',
  },
);

export const FilmSwiperScreen = createStackNavigator(
  {
    FilmSwiper: {
      screen: FilmSwiperListScreen,
      navigationOptions: {
        headerShown: false,
      },
    },
    FilmDetail: {
      screen: FilmDetailScreen,
      navigationOptions: {
        headerShown: false,
      },
    },
  },
  {
    initialRouteName: 'FilmSwiper',
  },
);

