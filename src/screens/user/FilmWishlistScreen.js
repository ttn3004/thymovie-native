import React, {useState, useEffect} from 'react';
import {Container, Spinner} from 'native-base';

import {View, Modal} from 'react-native';
import {filmItemApi} from './../../api/filmApi';
import FilmDetail from './../../components/film/FilmDetail';
import {isEmptyObj} from './../../utils/JsonUtils';
import FilmDetailHeader from './../../components/film/FilmDetailHeader';

const FilmWishlistScreen = props => {
  const [filmId, setFilmId] = useState(props.navigation.state.params.filmId);
  const [isLoading, setIsLoading] = useState(false);
  const [filmItem, setFilmItem] = useState({});

  useEffect(() => {
    console.debug('FilmWishlistScreen', filmId);
    async function fetchData() {
      setIsLoading(true);
      try {
        const data = await filmItemApi(filmId);
        setFilmItem(data);
      } catch (error) {
        console.error('FilmWishlistScreen', error);
        setIsLoading(false);
      } finally {
        setIsLoading(false);
      }
    }
    fetchData();
    
  }, [filmId]);

  return (
    <Container>
      <Modal animationType="fade" transparent={true} visible={isLoading}>
        <View
          style={{
            flex: 1,
            backgroundColor: 'rgba(0,0,0,0.7)',
            justifyContent: 'center',
            alignItems: 'center',
          }}>
          <View>
            <Spinner />
          </View>
        </View>
      </Modal>
      {!isEmptyObj(filmItem) && (
        <React.Fragment>
          <FilmDetailHeader
            filmId={filmId}
            title={filmItem.title}
            navigation={props.navigation}
            fromRouteName={props.navigation.state.params.fromRouteName}
          />
          <FilmDetail filmItem={filmItem} />
        </React.Fragment>
      )}
    </Container>
  );
};
export default FilmWishlistScreen;
