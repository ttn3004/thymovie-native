import {createStackNavigator} from 'react-navigation-stack';
import WishListHomeScreen from './WishListHomeScreen';
import FilmWishlistScreen from './FilmWishlistScreen';
import FilmDetailScreen from './../film/FilmDetailScreen'
export const WishListScreen = createStackNavigator(
  {
    WishListHome: {
      screen: WishListHomeScreen,
      navigationOptions: {
        headerShown: false,
      },
    },
    FilmWishlistScreen: {
      screen: FilmWishlistScreen,
      navigationOptions: {
        headerShown: false,
      },
    },
  },
  {
    initialRouteName: 'WishListHome',
  },
);
