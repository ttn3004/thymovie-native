import React, {useState, useEffect} from 'react';
import {useSelector, useDispatch} from 'react-redux';
import WishlistHeader from './../../components/WishlistHeader';
import {View, Modal} from 'react-native';

import {
  Container,
  Text,
  Content,
  Separator,
  List,
  ListItem,
  Left,
  Thumbnail,
  Body,
  Right,
  Button,
  Icon,
  Badge,
  Spinner,
} from 'native-base';
import {
  IMG_LARGE_FILM,
  IMG_LARGE_SERIE,
  IMG_SERIE,
  IMG_FILM,
  LIMIT_LENGTH_WISHLIST,
  LIMIT_LENGTH_HOME,
} from './../../constants/config';
import {limitLength, isEmptyObj} from './../../utils/JsonUtils';
import {deleteWishlist, deleteWishlistLine} from './../../actions/appAction';

const WishListHomeScreen = props => {
  const appState = useSelector(state => state.appReducer);
  console.log('WishListHomeScreen', appState.isLoading);
  const dispatch = useDispatch();
  const handleGoToDetail = item => {
    props.navigation.push('FilmWishlistScreen', {
      filmId: item.id,
      title: item.title,
      fromRouteName: 'WishListHome',
    });
  };
  const handleDeleteWishlist = async idWl => {
    console.log('handleDeleteWishlist');
    await dispatch(deleteWishlist(idWl, appState.tokenApp));
  };
  const handleDeleteWishlistLine = async idWlLine => {
    console.log('handleDeleteWishlistLine');
    await dispatch(deleteWishlistLine(idWlLine, appState.tokenApp));
  };
  return (
    <Container>
      <WishlistHeader />
      {!isEmptyObj(appState.userApp) && (
        <Content>
          {Array.isArray(appState.userApp.wishlists) &&
            appState.userApp.wishlists.map(wl => {
              return (
                <Content key={wl.id}>
                  <Separator bordered>
                    <ListItem icon>
                      <Left>
                        <Text>{wl.label}</Text>
                      </Left>
                      <Right>
                        <Button
                          transparent
                          onPress={() => handleDeleteWishlist(wl.id)}>
                          <Icon name="trash" />
                        </Button>
                      </Right>
                    </ListItem>
                  </Separator>
                  {wl.wistlist_lines.map(item => {
                    return (
                      <List key={item.id}>
                        <ListItem thumbnail>
                          <Left
                            style={{
                              flexDirection: 'column',
                              justifyContent: 'center',
                            }}>
                            <Thumbnail
                              source={{
                                uri: `${
                                  item.film_type_id === 12
                                    ? IMG_LARGE_SERIE
                                    : IMG_LARGE_FILM
                                }${item.idcine}/${item.images_path}`,
                              }}
                            />
                            <Button
                              transparent
                              onPress={() =>
                                handleDeleteWishlistLine(item.wishlist_line_id)
                              }>
                              <Icon name="close" />
                            </Button>
                          </Left>
                          <Body>
                            <Text>{item.title}</Text>
                            <Text note>
                              {limitLength(
                                item.synopsis,
                                LIMIT_LENGTH_WISHLIST,
                              )}
                            </Text>
                          </Body>
                          <Right>
                            <Button
                              transparent
                              onPress={() => handleGoToDetail(item)}>
                              <Icon name="arrow-round-forward" />
                            </Button>
                          </Right>
                        </ListItem>
                      </List>
                    );
                  })}
                </Content>
              );
            })}
{/*           <Modal
            animationType="fade"
            transparent={true}
            visible={appState.isLoading}>
            <View
              style={{
                flex: 1,
                backgroundColor: 'rgba(0,0,0,0.7)',
                justifyContent: 'center',
                alignItems: 'center',
              }}>
              <View>
                <Spinner />
              </View>
            </View>
          </Modal> */}
        </Content>
      )}
    </Container>
  );
};
export default WishListHomeScreen;
