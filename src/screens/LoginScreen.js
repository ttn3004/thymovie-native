import React, {useState, useEffect} from 'react';
import {useSelector, useDispatch} from 'react-redux';
import {View, Image, Modal, StyleSheet} from 'react-native';
import {
  Container,
  Content,
  Form,
  Item,
  Input,
  Label,
  Button,
  Text,
  Spinner,
  Left,
  Header,
  Body,
  Title,
  Icon,
} from 'native-base';
import {doUserLogin, doUserRegister} from './../actions/appAction';
import {isEmptyObjByAttr} from './../utils/JsonUtils';
const LoginScreen = props => {
  /*  const [email, setEmail] = useState('mailtranvanvuong@gmail.com');
  const [password, setPassword] = useState('123'); */

  const [email, setEmail] = useState('');
  const [password, setPassword] = useState('');

  const appState = useSelector(state => state.appReducer);
  const dispatch = useDispatch();

  useEffect(() => {
    //console.debug('LoginScreen', appState);
  });
  async function handleLogin() {
    await dispatch(doUserLogin(email, password));
    if (appState.tokenApp) {
      props.navigation.navigate('App');
    }
  }
  async function handleRegister() {
    await dispatch(doUserRegister(email, password));
    props.navigation.navigate('App');
  }
  return (
    <Container>
      <Header>
        <Left>
          <Image
            source={require('./../static/logo_04A.png')}
            style={{width: 30, height: 30}}
          />
        </Left>
        <Body>
          <Title>Bienvenue à thymovie.com</Title>
        </Body>
      </Header>
      <Content>
        <Form>
          <Item inlineLabel>
            <Label>Username</Label>
            <Input
              keyboardType="email-address"
              autoCapitalize="none"
              value={email}
              onChangeText={email => setEmail(email)}
            />
          </Item>
          <Item inlineLabel last>
            <Label>Password</Label>
            <Input
              secureTextEntry={true}
              value={password}
              onChangeText={password => setPassword(password)}
            />
          </Item>
          <Button full onPress={handleLogin}>
            <Text>Connecter</Text>
          </Button>
          <Button full success onPress={handleRegister} style={{marginTop: 40}}>
            <Text>Créer un compte</Text>
          </Button>
        </Form>
        {!isEmptyObjByAttr(appState, 'error') && (
          <View
            style={{
              flex: 1,
              justifyContent: 'center',
              alignItems: 'center',
            }}>
            <Text style={styles.textWarn}>
              <Icon name="warning" /> {appState.error}
            </Text>
          </View>
        )}
      </Content>
      <Modal
        animationType="fade"
        transparent={true}
        visible={appState.isLoading}>
        <View
          style={{
            flex: 1,
            backgroundColor: 'rgba(0,0,0,0.7)',
            justifyContent: 'center',
            alignItems: 'center',
          }}>
          <View>
            <Spinner />
          </View>
        </View>
      </Modal>
    </Container>
  );
};
const styles = StyleSheet.create({
  textWarn: {
    color: 'red',
    marginTop: 40
  },
});
export default LoginScreen;
