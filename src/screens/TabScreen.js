import React from 'react';

import {createBottomTabNavigator} from 'react-navigation-tabs';

import {WishListScreen} from './user/index.js';
import {Icon} from 'native-base';
import {FilmSwiperScreen, FilmScreen} from './film/index.js';
import {StackActions} from 'react-navigation';
const tabBarOnPress = ({navigation, defaultHandler}) => {
  const {isFocused, state, goBack} = navigation;
  console.log('tabBarOnPress', state);
  // to navigate to the top of stack whenever tab changes
  navigation.dispatch(StackActions.popToTop());
  defaultHandler();
};

export const TabScreen = createBottomTabNavigator(
  {
    Home: {
      screen: FilmSwiperScreen,
      navigationOptions: {
        tabBarLabel: 'Accueil',
        showIcon: true,
        tabBarIcon: () => {
          return <Icon name="home" />;
        },
        tabBarOnPress,
      },
    },
    FilmHome: {
      screen: FilmScreen,
      navigationOptions: {
        tabBarLabel: 'Cinéma',
        showIcon: true,
        tabBarIcon: () => {
          return <Icon name="aperture" />;
        },
        tabBarOnPress,
      },
    },
    SerieList: {
      screen: FilmScreen,
      navigationOptions: {
        tabBarLabel: 'Séries',
        showIcon: true,
        tabBarIcon: () => {
          return <Icon name="film" />;
        },
        tabBarOnPress,
      },
    },
    Wishlist: {
      screen: WishListScreen,
      navigationOptions: {
        tabBarLabel: 'Wishlist',
        showIcon: true,
        tabBarIcon: () => {
          return <Icon name="heart" />;
        },
      },
      tabBarOnPress,
    },
  },
  {
    tabBarOptions: {
      activeTintColor: 'red',
      inactiveTintColor: 'grey',
      showIcon: true,
    },
  },
);
