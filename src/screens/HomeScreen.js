import React, {useState, useEffect} from 'react';
import {useSelector, useDispatch} from 'react-redux';
import DeskSwiperHome from './../components/DeckSwiperHome';

import {Container, TabHeading, Text, Tab, Tabs} from 'native-base';
import {doFilmHome} from '../actions/filmListAction';

import HeaderComponent from '../components/HeaderComponent';
const HomeScreen = props => {
  const filmHome = useSelector(state => state.filmHomeReducer);
  const dispatch = useDispatch();
  useEffect(() => {
    if (filmHome.filmList.length === 0) dispatch(doFilmHome());
  }, [dispatch]);

  function filterFilmList(filmList, typeFilm) {
    return filmList.filter(function(item) {
      return item.bests === typeFilm;
    });
  }

  return (
    <Container>
      <HeaderComponent navigation={props.navigation} />

      {filmHome.filmList.length > 0 && (
        <Tabs locked={true}>
          <Tab
            heading={
              <TabHeading>
                <Text style={{textAlign: 'center'}}>Meilleurs films</Text>
              </TabHeading>
            }>
            <DeskSwiperHome  navigation={props.navigation}
              filmList={filterFilmList(filmHome.filmList, 'BESTS')}
            />
          </Tab>
          <Tab
          heading={
              <TabHeading>
                <Text style={{textAlign: 'center'}}>Au cinéma</Text>
              </TabHeading>
            }>
            <DeskSwiperHome  navigation={props.navigation}
              filmList={filterFilmList(filmHome.filmList, 'ATCINEMA')}
            />
          </Tab>
          <Tab
           heading={
              <TabHeading>
                <Text style={{textAlign: 'center'}}>Avant-premières</Text>
              </TabHeading>
            }>
            <DeskSwiperHome  navigation={props.navigation}
              filmList={filterFilmList(filmHome.filmList, 'PREVIEW')}
            />
          </Tab>
          <Tab heading={
              <TabHeading>
                <Text style={{textAlign: 'center'}}>Meilleurs séries</Text>
              </TabHeading>
            }>
            <DeskSwiperHome  navigation={props.navigation}
              filmList={filterFilmList(filmHome.filmList, 'BESTSERIE')}
            />
          </Tab>
        </Tabs>
      )}
    </Container>
  );
};
export default HomeScreen;
