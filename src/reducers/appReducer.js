import {
  AUTH_START,
  USER_ACTION_START,
  USER_ACTION_FAILURE,
  AUTH_SUCCESS,
  AUTH_LOGIN_SUCCESS,
  AUTH_LOGOUT_SUCCESS,
  AUTH_FAILURE,
} from '../constants/actionTypes';

const initialState = {
  userApp: {},
  isLoading: false,
  tokenApp: '',
  error: '',
};
const appReducer = (state = initialState, action) => {
  switch (action.type) {
    case AUTH_START:
      return {
        ...state,
        isLoading: true,
        userApp: {},
        tokenApp: '',
        error: '',
      };
    case AUTH_SUCCESS:
      return {
        ...state,
        userApp: action.data,
        isLoading: false,
        error: '',
      };
    case AUTH_LOGIN_SUCCESS:
      return {
        ...state,
        tokenApp: action.data,
        userApp: {},
        isLoading: false,
        error: '',
      };
    case AUTH_LOGOUT_SUCCESS:
      return {
        ...state,
        tokenApp: '',
        userApp: {},
        isLoading: false,
        error: '',
      };
    case AUTH_FAILURE:
      return {
        ...state,
        isLoading: false,
        userApp: {},
        tokenData: '',
        error: action.error,
      };
    case USER_ACTION_START:
      return {
        ...state,
        isLoading: true,
        error: '',
      };
    case USER_ACTION_FAILURE:
      return {
        ...state,
        isLoading: false,
        error: '',
      };
    default: {
      return state;
    }
  }
};
export default appReducer;
