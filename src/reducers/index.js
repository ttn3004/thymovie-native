import { combineReducers } from 'redux';
import { persistReducer } from "redux-persist";
import AsyncStorage from '@react-native-community/async-storage';
import appReducer from './appReducer'
import filmHomeReducer from './filmHomeReducer'
import filmListReducer from './filmListReducer'
const persistConfig = {
	key: "brkp",
	storage: AsyncStorage,
	blacklist: ['filmHomeReducer','filmListReducer']
};

const rootReducer = combineReducers ({
	appReducer,
	filmHomeReducer,
	filmListReducer
});

const persistedReducer = persistReducer(persistConfig, rootReducer);

export default persistedReducer;