import {FILM_HOME_START,FILM_HOME_SUCCESS,FILM_HOME_FAILURE} from '../constants/actionTypes'

const initialState = {
    filmList: [],
    isLoading: false,
    error:''
  };
  const filmHomeReducer = (state = initialState, action) => {
    switch (action.type) {
      case FILM_HOME_START:
        return {
          ...state,
          isLoading: true,
          filmList:[],
          error:''
        };
      case FILM_HOME_SUCCESS:
        return {
          ...state,
          isLoading: false,
          filmList: action.data
        };
      case FILM_HOME_FAILURE:
        return {
          ...state,
          isLoading: false,
          error: action.error,
          filmList:[],
        };
      default:
        return state;
    }
  };
  export default filmHomeReducer;