import {
  FILM_HOME_START,
  FILM_LIST_SUCCESS,
  FILM_INIT_START,
  FILM_HOME_FAILURE,
} from '../constants/actionTypes';
const initialState = {
  isLoading: false,
  error: '',
  filmArray: [],
  postDataTableObject: {
    page: 0,
    totalPage: 0,
    pageSize: 10,
    totalSize: 0,
    filmType: 'all',
    sorted: [],
    filtered: [],
    menuFiltered: [],
    listFiltered: [],
    searchByKeyword: '',
  },
};
const filmListReducer = (state = initialState, action) => {
  switch (action.type) {
    case FILM_HOME_START:
      return {
        ...state,
        isLoading: true,
        postDataTableObject: {
          ...state.postDataTableObject,
          listFiltered: [],
        },
      };
    case FILM_INIT_START:
      return {
        ...state,
        isLoading: true,
        filmArray: [],
        postDataTableObject: {
          ...state.postDataTableObject,
          listFiltered: [],
        },
      };
    case FILM_LIST_SUCCESS:
      return {
        ...state,
        isLoading: false,
        postDataTableObject: action.data,
        filmArray: [...state.filmArray, ...action.data.listFiltered],
      };
    case FILM_HOME_FAILURE:
      return {
        ...state,
        isLoading: false,
        error: action.error,
        postDataTableObject: {},
      };
    default:
      return  {
        ...state,
        isLoading: false
      };;
  }
};
export default filmListReducer;
