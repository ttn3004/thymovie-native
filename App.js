import React from 'react';
import {createAppContainer, createSwitchNavigator} from 'react-navigation';
import {Provider} from 'react-redux';
import {PersistGate} from 'redux-persist/integration/react';
import LoginScreen from './src/screens/LoginScreen';
import {TabScreen} from './src/screens/TabScreen';
import {createStackNavigator} from 'react-navigation-stack';
import AuthLoadingScreen from './src/screens/AuthLoadingScreen';
import {store, persistor} from './src/store';

const AuthStack = createStackNavigator({
  Login: {
    screen: LoginScreen,
    navigationOptions: {
      // headerTitle instead of title
      headerShown: false,
    },
  },
});

const AppStack = createStackNavigator({
  TabScreen: {
    screen: TabScreen,
    navigationOptions: {
      headerShown: false,
    },
  },
});

let Navigation = createAppContainer(
  createSwitchNavigator({
    Starter: AuthLoadingScreen,
    App: AppStack,
    Auth: AuthStack,
  }),
);

// Render the app container component with the provider around it
const App = () => {
  return (
    <Provider store={store}>
      <PersistGate loading={null} persistor={persistor}>
        <Navigation />
      </PersistGate>
    </Provider>
  );
};
export default App;
